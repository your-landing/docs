Created in the `_projects` folder, files are created by numbers, starting with the number 1 (`1.md`, `2.md`, etc.). You can create words and as you like, will be sorted alphabetically on the page.

- [How to setup Projects on the landing page?](blocks/projects.md)
- [How to change the text on the first button with categories?](config.md#projects)

## Blockquote

![blockquote](home/blockquote.jpg)

---

`layout` this is a template for the project page (do not change it if you do not know how to use it), takes the value:
- project

---

`blockquote` turns a block into a blockquote, takes on the value of boolean

---

`category` (optional) this is filtering by categories of your projects, which will also be displayed on the main page of your landing page

![category]

---

background: warning
align: center

---

`title` blockquote signature, takes the value of text

---

`subtitle` (optional) in italic, takes the value of text

---

**content** - your quote, it may be large, but the main page with projects will include only the first paragraph

##### Example
```yaml
---
layout: project
blockquote: true
category: ['Blockquote']
title: Someone famous in
subtitle: Source Title
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.
```
```yaml
---
layout: project
blockquote: true
category: ['Blockquote']
background: warning
align: center
title: Someone famous in
subtitle: Source Title
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.
```

## Normal project

![projects](home/projects.jpg)

---

`layout` this is a template for the project page (do not change it if you do not know how to use it), takes the value:
- project

---

`category` (optional) this is filtering by categories of your projects, which will also be displayed on the main page of your landing page

![category]

---

`title` takes the value of text

---

`image` (optional) image for your project in the site folder, takes the value of a reference

---

`description` (optional) determines to show the first paragraph of content on the main page, takes on the value of boolean

---

`block` (optional) makes a block only from a image, takes on the value of boolean

---

**content** - your content, it may be large, but the main page with projects will include only the first paragraph

##### Example
```yaml
---
layout: project
category: ['Photography']
title: On the street
image: 'assets/projects/people-1.jpg'
---
```
```yaml
---
layout: project
category: ['Art']
title: Beach
image: 'assets/projects/nature-1.jpg'
description: true
---

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia nesciunt ea inventore animi, id nihil!
```
```yaml
---
layout: project
category: ['Сulture']
title: On the street
image: 'assets/projects/arch-1.jpg'
block: true
---
```

[category]: home/category.png