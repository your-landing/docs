## General
Default settings for jekyll

---

`title` title for the site, takes the value of text

---

`description` site description, takes the value of text

---

`baseurl` (optional) the subpath of your site

---

`url` (optional) the base hostname & protocol for your site, takes the value of a reference

##### Example
```yaml
title: Your awesome title
description: >-
  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, dolores aliquam doloribus expedita error quod eos, inventore autem est ducimus numquam quisquam, deserunt saepe in quasi excepturi accusamus quas optio.
baseurl: '/example-1'
url: 'https://your-landing.gitlab.io'
```

## Header

`author` name of the author of the site, takes the value of text

---

`favicon` takes the list value:  
`url` image for your site in the site folder  
`type` favicon type, takes the value:
- .ico - image/x-icon
- .png - image/png
- .gif - image/gif

[More](https://en.wikipedia.org/wiki/Favicon)

---

`social` takes the list value:  
`image` image for OpenGraph in the site folder  
`type` OpenGraph image type, takes the value:
- .png - image/png
- .jpg/.jpeg - image/jpeg
- .svg - image/svg+xml
- .webp - image/webp
- .gif - image/gif

[More](https://www.iana.org/assignments/media-types/media-types.xhtml#image)

`width` width for image  
`height` height for image

---

`analytics` if you want to enable analytics, set it to `true`, and paste the analytics code into the `_include/analytics.html` file

##### Example
```yaml
author: Juan King

favicon:
  url: 'favicon.ico'
  type: 'image/x-icon'

social:
  image: 'assets/placeholder.png'
  type: 'image/png'
  width: 1200
  height: 630

analytics: false
```

## Navbar

`brand` takes the list value:  
`type` the type of brand that will be displayed, takes the list value:
- title (the navbar will display the title which is the lines above)
- image (a image will be displayed in the navbar)
- text (the text you write below will be displayed)

`image` image for navbar, in the site folder (4:1, e.g. 120x30px), will work if you select a above value 'image'  
`text` text for navbar, will work if you select a above value 'text'

---

navbar has 3 types of links:

| [1 type: Normal url](#1-type-normal-url) | [2 type: Dropdown list](#2-type-dropdown-list) | [3 type: Dropdown description](#3-type-dropdown-description) |
| :-: | :-: | :-: |
| ![navbar-1](home/navbar-1.png) | ![navbar-2](home/navbar-2.png) | ![navbar-3](home/navbar-3.png) |

`navbar` takes the list value:

#### 1 type: Normal url

`name` url title, takes the value of text  
`url` takes the value of a reference

##### Example
```yaml
navbar:
  - name: Home
    url: 'https://example.com/home'
```

#### 2 type: Dropdown list

`name` the title of the list, takes the value of text  
`dropdown` takes the list value:  
`name` url title, if you don't write the url after this value, the value will be used as the child list header, takes the value of text  

![dropdown-1](home/dropdown-2.png)

`url` takes the value of a reference  
`active` makes a link in the list active or disabled, takes the value:
- true
- false
- null

`divider` if you enable this value, a line appears after the url, takes on the value of boolean

![dropdown-2](home/dropdown-2.png)

##### Example
```yaml
navbar:
  - name: Services
    dropdown:
      - name: Header One
      - name: Contacts
        url: 'https://example.com/#contacts'
        active: true
      - name: Portfolio
        url: '#portfolio'
        active: false
        divider: true
      - name: Header Two
      - name: Translate
        url: 'https://example.com/translate'
```

#### 3 type: Dropdown description

`name` the title of the list, takes the value of text  
`description` description in the dropdown list, takes the value of text

##### Example
```yaml
navbar:
  - name: Description
    description: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, temporibus.
```

---

`navbar_color` (optional, default: light) text color in navbar, takes the value:
- dark
- light

## Blog

`blog_button` (optional, default: Read more) button text in the blog block

## Projects

`projects_button` (optional, default: All) button text in the projects block

## Footer

`copyright` (optional) if you are a big project and started a long time ago, you can specify your start year, takes on the value of integer

---

`footer:` (optional) takes the list value:  
`name` (optional) the name of the list, takes the value of text  
`links` takes the list value:  
`name` url title, takes the value of text  
`url` takes the value of a reference

##### Example
```yaml
footer:
  - name: Features
    links:
      - name: Cool stuff
        url: '#'
      - name: Random feature
        url: '#'
      - name: Team feature
        url: '#'
      - name: Stuff for developers
        url: '#'
      - name: Another one
        url: '#'
      - name: Last time
        url: '#'
  - name: Resources
    links:
      - name: Business
        url: '#'
      - name: Education
        url: '#'
      - name: Government
        url: '#'
      - name: Gaming
        url: '#'
      - name: Resource
        url: '#'
      - name: Another resource
        url: '#'
      - name: Final resource
        url: '#'
  - name: About
    links:
      - name: Team
        url: '#'
      - name: Locations
        url: '#'
      - name: Privacy
        url: '#'
      - name: Terms
        url: '#'
```

## Navbar example
```yaml
# General
title: Your awesome title
description: >-
  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, dolores aliquam doloribus expedita error quod eos, inventore autem est ducimus numquam quisquam, deserunt saepe in quasi excepturi accusamus quas optio.
baseurl: '/example-1'
url: 'https://your-landing.gitlab.io'


# Header
author: Juan King

favicon:
  url: 'favicon.ico'
  type: 'image/x-icon'

social:
  image: 'assets/placeholder.png'
  type: 'image/png'
  width: 1200
  height: 630

analytics: false


# Navbar
brand:
  type: title
  image: 'assets/placeholder-brand.jpg'
  text: Example Title
navbar:
  - name: Home
    url: '#'
  - name: About
    url: '#about'
  - name: Services
    dropdown:
      - name: Title One
      - name: Contacts
        url: '#contacts'
        active: true
      - name: Portfolio
        url: '#portfolio'
        active: false
        divider: true
      - name: Title Two
      - name: Translate
        url: '#translate'
        active: null
  - name: Description
    description: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, temporibus.
  - name: Pricing
    url: '#pricing'
  - name: <i class="fas fa-shopping-cart"></i>
    url: '#shop'
navbar_color: dark


# Blog
blog_button: Read more


# Projects
projects_button: All


# Footer
copyright: '2010'
footer:
  - name: Features
    links:
      - name: Cool stuff
        url: '#'
      - name: Random feature
        url: '#'
      - name: Team feature
        url: '#'
      - name: Stuff for developers
        url: '#'
      - name: Another one
        url: '#'
      - name: Last time
        url: '#'
  - name: Resources
    links:
      - name: Business
        url: '#'
      - name: Education
        url: '#'
      - name: Government
        url: '#'
      - name: Gaming
        url: '#'
      - name: Resource
        url: '#'
      - name: Another resource
        url: '#'
      - name: Final resource
        url: '#'
  - name: About
    links:
      - name: Team
        url: '#'
      - name: Locations
        url: '#'
      - name: Privacy
        url: '#'
      - name: Terms
        url: '#'
```