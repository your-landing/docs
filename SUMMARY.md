# Summary

* [Home](README.md)
* [Config](config.md)
* [Blocks]()
  * [BigCenter](blocks/bigcenter.md)
  * [Blog](blocks/blog.md)
  * [Carousel](blocks/carousel.md)
  * [Projects](blocks/projects.md)
* [Blog](blog.md)
* [Projects](projects.md)

---

* [Your Landing](https://your-landing.gitlab.io)
* [Example 1](https://your-landing.gitlab.io/example-1/)