![blog](home/blog.png)

Created in the `_posts` folder, files are created just like in a normal jekyll blog, e.g. `YYYY-MM-DD-SLUG.md`

- [How to change the 'Read more' button in a blog?](config.md#blog)

---

`layout` this is a template for the blog page (do not change it if you do not know how to use it), takes the value:
- blog

---

`title` takes the value of text

---

`date` this is the date the post was created, takes the date value, e.g. `YYYY-MM-DD HH:MM:SS +/-ZZZZ`

---

`category` (optional) this is filtering by categories of your blog, takes the value of text

---

`image` (optional) image for your blog in the site folder, takes the value of a reference

##### Example
```yaml
---
layout: blog
title: "Fully responsive"
date: 2020-01-05 21:12:00 +0300
category: Test
image: 'assets/blog/2.png'
---
```