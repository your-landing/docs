![bigcenter](bigcenter.jpg)

Created in the `_blocks` folder, files are created by numbers, starting with the number 1 (`1.md`, `2.md`, etc.). You can create words and as you like, will be sorted alphabetically on the page.

---

`block` takes the values:
- bigcenter (in this case)
- blog
- carousel
- projects

---

`display` (optional) is the block size, takes the value:
- 25
- 50
- 75
- 100
- auto

[More](https://getbootstrap.com/docs/4.1/utilities/sizing/)

---

`background` (optional) block background, takes the value:
- 'assets/placeholder.png' (image in the site folder)
- '#cccccc' (hex color code, also accepts 3 characters)
- 'E6E6E6' (hex color code, also accepts 3 characters)

---

`color` (optional) the color of the text block, takes the value:
- primary
- secondary
- success
- danger
- warning
- info
- light
- dark
- body
- muted
- white
- black-50
- white-50

[More](https://getbootstrap.com/docs/4.1/utilities/colors/#color)

---

`suptitle` (optional) small text before the main title, takes the value of text

---

`title` (optional) main title, takes the value of text

---

`description` (optional) description under the title, takes the value of text

---

`buttons` (optional) takes the list value:  
`name` text in the button, takes the value of text  
`url` link when you click the button, takes the value of a reference  
`color` (optional, default: primary) the color of the button, takes the same value as the color above  
`outline` (optional) makes the button transparent with a border, takes on the value of boolean

##### Example
```yaml
---
block: bigcenter
display: 100
background: 'assets/placeholder.png'
color: light
suptitle: Template for
title: Your landing
description: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro doloremque facilis, cum est tenetur dicta excepturi maxime nihil voluptatibus dolores? Reiciendis incidunt laboriosam possimus ipsam, eum, hic repudiandae soluta dicta.
buttons:
  - name: Learn more
    url: 'https://gitlab.com/your-landing/example'
    color: primary
    outline: false
  - name: Demo
    url: 'https://your-landing.gitlab.io/example-1/'
    color: light
    outline: true
---
```