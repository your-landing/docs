![blog](blog.jpg)

Created in the `_blocks` folder, files are created by numbers, starting with the number 1 (`1.md`, `2.md`, etc.). You can create words and as you like, will be sorted alphabetically on the page. [Learn more about blog](../blog.md).

---

`block` takes the values:
- bigcenter
- blog (in this case)
- carousel
- projects

---

`background` (optional) block background, takes the value:
- 'assets/placeholder.png' (image in the site folder)
- '#cccccc' (hex color code, also accepts 3 characters)
- 'E6E6E6' (hex color code, also accepts 3 characters)

---

`color` (optional) the color of the text block, takes the value:
- primary
- secondary
- success
- danger
- warning
- info
- light
- dark
- body
- muted
- white
- black-50
- white-50

[More](https://getbootstrap.com/docs/4.1/utilities/colors/#color)

---

`title` (optional) takes the value of text

---

`description` (optional) description under the title, takes the value of text

##### Example
```yaml
---
block: blog
background: '#e6e6e6'
color: dark
title: Your blog
description: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit quia aut, placeat unde saepe ipsum est non libero sapiente sit.
---
```