![projects](projects.jpg)

Created in the `_blocks` folder, files are created by numbers, starting with the number 1 (`1.md`, `2.md`, etc.). You can create words and as you like, will be sorted alphabetically on the page. [Learn more about Projects](../projects.md).

---

`block` takes the values:
- bigcenter
- blog
- carousel
- projects (in this case)

---

`background` (optional) block background, takes the value:
- 'assets/placeholder.png' (image in the site folder)
- '#cccccc' (hex color code, also accepts 3 characters)
- 'E6E6E6' (hex color code, also accepts 3 characters)

---

`color` (optional) the color of the text block, takes the value:
- primary
- secondary
- success
- danger
- warning
- info
- light
- dark
- body
- muted
- white
- black-50
- white-50

[More](https://getbootstrap.com/docs/4.1/utilities/colors/#color)

---

`title` (optional) takes the value of text

---

`description` (optional) description under the title, takes the value of text

---
`more` (optional) takes the list value:  
`text` (optional) description at the bottom of the block

`buttons` takes the list value:  
`name` text in the button, takes the value of text  
`url` link when you click the button, takes the value of a reference  
`color` (optional, default: primary) the color of the button, takes the same value as the color above  
`outline` (optional) makes the button transparent with a border, takes on the value of boolean

##### Example
```yaml
---
block: projects
background: 'fff'
color: dark
title: Your project
description: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum iusto culpa, repellat delectus, explicabo debitis velit eos. At ullam vitae, facilis!
more:
  text: Want to see more projects? Just press “All Projects” on the right side!
  button:
    name: All Projects
    url: 'https://example.com'
    color: primary
    outline: false
---
```