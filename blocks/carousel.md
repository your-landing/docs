![carousel](carousel.jpg)

Created in the `_blocks` folder, files are created by numbers, starting with the number 1 (`1.md`, `2.md`, etc.). You can create words and as you like, will be sorted alphabetically on the page.

---

`block` takes the values:
- bigcenter
- blog
- carousel (in this case)
- projects

---

`images` takes the list value:  
`url` image in the site folder, takes the value of a reference  
`title` (optional) takes the value of text  
`description` (optional) description under the title, takes the value of text

##### Example
```yaml
---
block: carousel
images:
  - url: 'assets/carousel/1.jpeg'
    title: Сathedral
    description: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste sequi aliquid error, molestiae voluptas in. Molestiae facere porro, ad aut.
  - url: 'assets/carousel/2.jpeg'
    title: Lighthouse
    description: Ipsum dolor sit amet, consectetur adipisicing elit. Iste sequi aliquid error, molestiae voluptas in. Molestiae facere porro, ad aut.
  - url: 'assets/carousel/3.jpeg'
    title: Roofs
    description: Dolor sit amet, consectetur adipisicing elit. Iste sequi aliquid error, molestiae voluptas in. Molestiae facere porro, ad aut.
---
```